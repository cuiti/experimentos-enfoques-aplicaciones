-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local widget = require( "widget" )

local t = display.newText( "Waiting for button event...", 0, 0, native.systemFont, 18 )
t.x, t.y = display.contentCenterX, 70

local function onSystemEvent( event )
	if event.type == "applicationStart" then
		t.text =  " hello world " ;
		-- media.playVideo( "vid.mp4", true )
	end
end
Runtime:addEventListener( "system", onSystemEvent )