﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace TestSpeedWithXamarin
{
    [Activity(Label = "TestBattteryWithXamarin", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private Button boton;
        private TextView resultado;

        protected override void OnCreate( Bundle bundle )
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            boton = FindViewById<Button>(Resource.Id.botonProcesar);

            resultado = FindViewById<TextView>(Resource.Id.resultado);
			this.funcion();


        }
		protected  void funcion()
		{
			DateTime t1, t2;
			double serie;
			t1 = DateTime.Now;
			serie = 0;
			for (int j = 1; j <= 10; j++)
			{
				for (int k = 1; k <= 1000000; k++)
				{
					serie = serie + Math.Log(k) / Math.Log(2) + (3 * k / (2 * j)) + Math.Sqrt(k) + Math.Pow(k, j - 1);
				}
			}
			t2 = DateTime.Now;
			double milisegundos = t2.Subtract(t1).TotalMilliseconds;
			resultado.Text = string.Format("valor serie {0}, en {1} milisegundos", serie, milisegundos);

		}

			
    }
}

