import { Observable } from 'data/observable';

import * as app from 'application';

import { Page } from 'ui/page';

import { TNSPlayer, AudioPlayerOptions, AudioRecorderOptions } from 'nativescript-audio';

declare var android;

export class AudioDemo extends Observable {
  public isPlaying: boolean;
  public isRecording: boolean;
  public recordedAudioFile: string;
  private recorder;
  private player;
  private audioSessionId;
  private page;
  private audioUrls: Array<any> = [];
  private meterInterval: any;


  constructor(page: Page) {
    super();

    this.player = new TNSPlayer();
    this.playAudio();
  }


  /***** AUDIO PLAYER *****/

  public playAudio() {

    try {
      var playerOptions = {
        audioFile: "~/audio/audio.mp3",

        completeCallback: () => {

          this.player.dispose().then(() => {

            console.log('DISPOSED');
          }, (err) => {
            console.log('ERROR disposePlayer: ' + err);
          });
        },
        errorCallback: (errorObject) => {
          console.log(JSON.stringify(errorObject));

        },
        infoCallback: (args) => {
          console.log(JSON.stringify(args));
        }
      };

        this.player.playFromFile(playerOptions).then(() => {
          this.set("isPlaying", true);
        }, (err) => {
          console.log(err);
          this.set("isPlaying", false);
        });

    } catch (ex) {
      console.log(ex);
    }

  }


}
