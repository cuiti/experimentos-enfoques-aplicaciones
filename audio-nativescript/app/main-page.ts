import * as app from 'application';
import {Color} from 'color';
import {AudioDemo} from "./main-view-model";

function pageLoaded(args) {
  var page = args.object;
  page.bindingContext = new AudioDemo(page);

}
exports.pageLoaded = pageLoaded;
