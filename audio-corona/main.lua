
local function releaseAudio( event )
    if ( event.completed ) then
        audio.dispose( playing )
    else
        audio.dispose( playing )
    end
end
 
local audioFile = audio.loadStream( "audio.mp3" )
local playing = audio.play( audioFile, { onComplete=releaseAudio } )
